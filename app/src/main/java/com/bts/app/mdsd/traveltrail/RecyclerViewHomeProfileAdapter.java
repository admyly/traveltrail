package com.bts.app.mdsd.traveltrail;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bts.app.mdsd.traveltrail.Lastmint.LastmintOffer_Activity;
import com.bts.app.mdsd.traveltrail.Profile.Profile;
import com.bts.app.mdsd.traveltrail.Profile.UserDetailProfile;

import java.util.ArrayList;
import java.util.List;


public class RecyclerViewHomeProfileAdapter extends RecyclerView.Adapter<RecyclerViewHomeProfileAdapter.MyViewHolder> {

    private static final String TAG = "RecyclerViewAdapterHomeProfile";

    //vars
    private Context mContext ;
    private List<Profile> mData ;

    public RecyclerViewHomeProfileAdapter(HomeActivity homeActivity , List<Profile> lstprofile) {

        this.mContext = homeActivity;
        this.mData = lstprofile;
    }




    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent , int viewType) {
        View view ;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.cardveiw_item_home_profile,parent,false);
        return new MyViewHolder (view);
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder , final int position) {

        Log.d (TAG , "onBindViewHolder: called.");

        holder.tv_profile_name.setText(mData.get(position).getName ());
        holder.tv_profile_type.setText(mData.get(position).getType ());
        holder.tv_profile_shortdesc.setText(mData.get(position).getDescription ());

        holder.img_profile_thumbnail.setImageResource(mData.get(position).getThumbnail());
        holder.img_profile_tag.setImageResource(mData.get(position).getMasterof ());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent (mContext , UserDetailProfile.class);

                // passing data to the book activity
                intent.putExtra ("Name" , mData.get (position).getName ());
                intent.putExtra ("Type" , mData.get (position).getType ());
                intent.putExtra ("Description" , mData.get (position).getDescription ());
                intent.putExtra ("Thumbnail" , mData.get (position).getThumbnail ());
                intent.putExtra ("Tag" , mData.get (position).getMasterof ());
                // start the activity
                mContext.startActivity (intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

                TextView tv_profile_name;
                TextView tv_profile_type;
                TextView tv_profile_shortdesc;
                ImageView img_profile_thumbnail;
                ImageView img_profile_tag;
                CardView cardView;

                public MyViewHolder(View itemView) {
                    super (itemView);

                    tv_profile_name = (TextView) itemView.findViewById (R.id.profile_name_id);
                    tv_profile_type = (TextView) itemView.findViewById (R.id.profile_type_user);
                    tv_profile_shortdesc = (TextView) itemView.findViewById (R.id.profile_desc);

                    img_profile_thumbnail = (ImageView) itemView.findViewById (R.id.profile_img_id);
                    img_profile_tag = (ImageView) itemView.findViewById (R.id.profile_tag);

                    cardView = (CardView) itemView.findViewById (R.id.cardview_id);

                }

    }

}
