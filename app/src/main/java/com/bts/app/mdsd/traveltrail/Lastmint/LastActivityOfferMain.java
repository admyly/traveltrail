package com.bts.app.mdsd.traveltrail.Lastmint;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.TextView;

import com.bts.app.mdsd.traveltrail.BottomNavigationViewHelper;
import com.bts.app.mdsd.traveltrail.HomeActivity;
import com.bts.app.mdsd.traveltrail.Notification.NotificationActivity;
import com.bts.app.mdsd.traveltrail.Profile.ProfileActivity;
import com.bts.app.mdsd.traveltrail.R;

import java.util.ArrayList;
import java.util.List;

public class LastActivityOfferMain extends AppCompatActivity {

    private TextView mTextMessage;
    private Intent i;
    private static final String TAG = "lastMintActivity";

    List<Book> lstBook ;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            switch (item.getItemId()) {
                case R.id.navigation_home:
                    mTextMessage.setText(R.string.title_home);
                    i = new Intent (LastActivityOfferMain.this, HomeActivity.class);
                    startActivity(i);
                    finish();
                    return true;

                case R.id.navigation_experience:
                    mTextMessage.setText(R.string.title_experience);
                    return true;

                case R.id.navigation_lastmint:
                    mTextMessage.setText(R.string.title_lastmintoffer);
                    i = new Intent(LastActivityOfferMain.this, LastActivityOfferMain.class);
                    startActivity(i);
                    finish();
                    return true;

                case R.id.navigation_notifications:
                    mTextMessage.setText(R.string.title_notifications);
                    i = new Intent(LastActivityOfferMain.this, NotificationActivity.class);
                    startActivity(i);
                    finish();
                    return true;

                case R.id.navigation_setting:
                    mTextMessage.setText(R.string.title_setting);
                    i = new Intent(LastActivityOfferMain.this, HomeActivity.class);
                    startActivity(i);
                    finish();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        BottomNavigationViewHelper.disableShiftMode(navigation);

        getlastmintoffer();

    }

    public void getlastmintoffer(){


        lstBook = new ArrayList<> ();
        lstBook.add(new Book("Vegan","Default","Description book",R.drawable.thevigitarian));
        lstBook.add(new Book("Vegetarian","Default","Description book",R.drawable.thewildrobot));
        lstBook.add(new Book("Event","Default","Description book",R.drawable.mariasemples));
        lstBook.add(new Book("Night Life","Default","Description book",R.drawable.themartian));
        lstBook.add(new Book("Adventure","Default","Description book",R.drawable.hediedwith));
        lstBook.add(new Book("Daily Offers","Default","Description book",R.drawable.thevigitarian));
        lstBook.add(new Book("Personal Offer","Default","Description book",R.drawable.thewildrobot));
        lstBook.add(new Book("Drinks","Default","Description book",R.drawable.mariasemples));
        lstBook.add(new Book("Soical","Default","Description book",R.drawable.themartian));


        RecyclerView myrv = (RecyclerView) findViewById(R.id.recyclerview_id);
        RecyclerViewAdapter myAdapter = new RecyclerViewAdapter(this,lstBook);
        myrv.setLayoutManager(new GridLayoutManager(this,3));
        myrv.setAdapter(myAdapter);


    }
}
