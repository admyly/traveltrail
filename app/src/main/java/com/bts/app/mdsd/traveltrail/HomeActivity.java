package com.bts.app.mdsd.traveltrail;


import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.bts.app.mdsd.traveltrail.Lastmint.LastActivityOfferMain;
import com.bts.app.mdsd.traveltrail.Notification.NotificationActivity;
import com.bts.app.mdsd.traveltrail.Profile.Profile;
import com.bts.app.mdsd.traveltrail.Profile.ProfileActivity;

import com.google.android.gms.maps.GoogleMap;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity {

    private TextView mTextMessage;
    private GoogleMap mMap;
    private Intent i;

    private static final String TAG = "HomeActivity";
    private static final String TAGNotification = "Notification Activiy";


    List<Profile> lstprofile ;

    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<String> mImageUrls = new ArrayList<>();


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            switch (item.getItemId()) {
                case R.id.navigation_home:
                    mTextMessage.setText(R.string.title_home);
                    return true;

                case R.id.navigation_experience:
                    mTextMessage.setText(R.string.title_experience);
                    i = new Intent (HomeActivity.this, ProfileActivity.class);
                    startActivity(i);
                    finish();
                    return true;

                case R.id.navigation_lastmint:
                    mTextMessage.setText(R.string.title_lastmintoffer);
                    i = new Intent(HomeActivity.this, LastActivityOfferMain.class);
                    startActivity(i);
                    finish();
                    return true;

                case R.id.navigation_notifications:
                    mTextMessage.setText(R.string.title_notifications);
                    i = new Intent(HomeActivity.this, NotificationActivity.class);
                    startActivity(i);
                    finish();
                    return true;

                case R.id.navigation_setting:
                    mTextMessage.setText(R.string.title_setting);
                    i = new Intent(HomeActivity.this, HomeActivity.class);
                    startActivity(i);
                    finish();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_home);

        getExperiences();
        initImageBitmaps();

        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        BottomNavigationViewHelper.disableShiftMode(navigation);
    }


    private void getExperiences(){

        Log.d(TAG, "initImageBitmaps: preparing bitmaps.");

        lstprofile= new ArrayList<> ();

        lstprofile.add(new Profile ("Boban Parson", "Local", "Travelling for 1 year around the world,", R.drawable.thevigitarian, R.drawable.ic_home_black_24dp , R.drawable.ic_home_black_24dp));
        lstprofile.add(new Profile ("Mary Chilling", "Wonderlust", "Travelling for 1 year around the world,", R.drawable.themartian, R.drawable.ic_home_black_24dp , R.drawable.ic_home_black_24dp));
        lstprofile.add(new Profile ("Jousha Ramada", "Traveler", "Travelling for 1 year around the world,", R.drawable.thewildrobot, R.drawable.ic_home_black_24dp , R.drawable.ic_home_black_24dp));
        lstprofile.add(new Profile ("Jousha Ramada", "Traveler", "Travelling for 1 year around the world,", R.drawable.privacy, R.drawable.ic_home_black_24dp , R.drawable.ic_home_black_24dp));

        RecyclerView myrv = (RecyclerView) findViewById(R.id.recyle_profile_activity);
        RecyclerViewHomeProfileAdapter myAdapter = new RecyclerViewHomeProfileAdapter (this,lstprofile);
        myrv.setLayoutManager(new GridLayoutManager (this,2));
        myrv.setAdapter(myAdapter);

    }

    private void initImageBitmaps(){

        Log.d(TAG, "initImageBitmaps: preparing bitmaps.");

        mImageUrls.add("https://c1.staticflickr.com/5/4636/25316407448_de5fbf183d_o.jpg");
        mNames.add("Bike for a rent, Less then Tourist Bike Station 5Euro for 6 hours, 10 Hours for 12 hours");

        mImageUrls.add("https://i.redd.it/tpsnoz5bzo501.jpg");
        mNames.add("Beers after workd at Bar49, El Born, 1 Euro for every 2nd beer, Only offered at Travel Trail");

        mImageUrls.add("https://i.redd.it/qn7f9oqu7o501.jpg");
        mNames.add("Locker Room : Home Locker in Marina Street 5 Euro To take care of your belonging");

        mImageUrls.add("https://i.redd.it/j6myfqglup501.jpg");
        mNames.add("Locker Room : Home Locker in Sant Antonio Street 6 Euro To take care of your belonging");


        initRecyclerView();
    }

    private void initRecyclerView(){

        Log.d(TAG, "initRecyclerView: init recyclerview.");

        RecyclerView recyclerView = findViewById(R.id.recyle_notification_activity);

        RecyclerViewAdapterHomeNotification adapter = new RecyclerViewAdapterHomeNotification (this, mNames, mImageUrls);

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager (this));
    }

}
