package com.bts.app.mdsd.traveltrail.Notification;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bts.app.mdsd.traveltrail.BottomNavigationViewHelper;
import com.bts.app.mdsd.traveltrail.HomeActivity;
import com.bts.app.mdsd.traveltrail.Lastmint.LastActivityOfferMain;
import com.bts.app.mdsd.traveltrail.Lastmint.LastMintActivity;
import com.bts.app.mdsd.traveltrail.Profile.ProfileActivity;
import com.bts.app.mdsd.traveltrail.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class NotificationActivity extends AppCompatActivity implements LocationListener {

    private static final String TAG = "Notification Activiy";

    private TextView mTextMessage;

    public static double tvLongi;
    public static double tvLati;

    TextView tvLatitude;
    TextView tvLongitude;

    LocationManager locationManager;

    private Intent i;
    Button button;
    TextView textView;

    //vars
    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<String> mImageUrls = new ArrayList<>();


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    mTextMessage.setText(R.string.title_home);
                    i = new Intent(NotificationActivity.this , HomeActivity.class);
                    startActivity(i);
                    finish();
                    return true;

                case R.id.navigation_experience:
                    mTextMessage.setText(R.string.title_experience);
                    i = new Intent(NotificationActivity.this , ProfileActivity.class);
                    startActivity(i);
                    finish();
                    return true;

                case R.id.navigation_lastmint:
                    mTextMessage.setText(R.string.title_lastmintoffer);
                    i = new Intent(NotificationActivity.this , LastActivityOfferMain.class);
                    startActivity(i);
                    finish();
                    return true;

                case R.id.navigation_notifications:

                    mTextMessage.setText(R.string.title_notifications);
                    i = new Intent(NotificationActivity.this , NotificationActivity.class);
                    startActivity(i);
                    finish();
                    return true;


                case R.id.navigation_setting:
                    mTextMessage.setText(R.string.title_setting);
                    Toast.makeText(getApplicationContext(), "No Activity Available on this click", Toast.LENGTH_LONG).show();
                    return true;
            }
            return false;
        }
    };

    @SuppressLint("ServiceCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        initImageBitmaps();
        CheckPermission();

        mTextMessage = (TextView) findViewById(R.id.message);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        BottomNavigationViewHelper.disableShiftMode(navigation);


    }

    @Override
    protected void onResume() {
        super.onResume();
        getLocation();
    }

    @Override
    protected void onPause() {
        super.onPause();
        locationManager.removeUpdates(this);
    }

    public void getLocation() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, this);
        } catch (SecurityException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Error" + e, Toast.LENGTH_LONG).show();
        }
    }

    private void CheckPermission() {

        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(),
                android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
        }
    }

    @Override
    public void onLocationChanged(Location location) {

        // Getting reference to TextView tv_longitude
        tvLongitude = (TextView) findViewById(R.id.tv_longitude);
        // Getting reference to TextView tv_latitude
        tvLatitude = (TextView) findViewById(R.id.tv_latitude);

        tvLongi = location.getLongitude();
        tvLati = location.getLatitude();

        // Setting Current Longitude
        tvLongitude.setText("Longitude:" + tvLongi);
        // Setting Current Latitude
        tvLatitude.setText("Latitude:" + tvLati);
        GeoAddress(location);

    }

    public void GeoAddress (Location location){


        try {
            Geocoder geocoder = new Geocoder(this);
            List<Address> addressList = null;
            addressList = geocoder.getFromLocation(tvLati,tvLongi,1);
            String street = addressList.get(0).getAddressLine(0);
            String country = addressList.get(0).getCountryName();

            tvLongitude.setText("Current Location : " + street);
            tvLatitude.setText("Country :  " + country);

        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Error" + e, Toast.LENGTH_LONG).show();
        }


    }

    @Override
    public void onStatusChanged(String provider , int status , Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        Toast.makeText(this, "Enabled new provider!" + provider, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderDisabled(String provider) {

        Toast.makeText(NotificationActivity.this, "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();

    }

    private void initImageBitmaps(){

        Log.d(TAG, "initImageBitmaps: preparing bitmaps.");

        mImageUrls.add("https://c1.staticflickr.com/5/4636/25316407448_de5fbf183d_o.jpg");
        mNames.add("Bike for a rent, Less then Tourist Bike Station 5Euro for 6 hours, 10 Hours for 12 hours");

        mImageUrls.add("https://i.redd.it/tpsnoz5bzo501.jpg");
        mNames.add("Trondheim");

        mImageUrls.add("https://i.redd.it/qn7f9oqu7o501.jpg");
        mNames.add("Portugal");

        mImageUrls.add("https://i.redd.it/j6myfqglup501.jpg");
        mNames.add("Rocky Mountain National Park");


        mImageUrls.add("https://i.redd.it/0h2gm1ix6p501.jpg");
        mNames.add("Mahahual");

        mImageUrls.add("https://i.redd.it/k98uzl68eh501.jpg");
        mNames.add("Frozen Lake");


        mImageUrls.add("https://i.redd.it/glin0nwndo501.jpg");
        mNames.add("White Sands Desert");

        mImageUrls.add("https://i.redd.it/obx4zydshg601.jpg");
        mNames.add("Austrailia");

        mImageUrls.add("https://i.imgur.com/ZcLLrkY.jpg");
        mNames.add("Washington");

        initRecyclerView();
    }

    private void initRecyclerView(){
        Log.d(TAG, "initRecyclerView: init recyclerview.");
        RecyclerView recyclerView = findViewById(R.id.recyle_profile_activity);
        NotificationDetiailActivity.RecyclerViewAdaptor adapter = new NotificationDetiailActivity.RecyclerViewAdaptor (this,mNames,mImageUrls);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager (this));
    }
}
