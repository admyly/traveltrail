package com.bts.app.mdsd.traveltrail;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bts.app.mdsd.traveltrail.Notification.NotificationDetiailActivity;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class RecyclerViewAdapterHomeNotification extends RecyclerView.Adapter<RecyclerViewAdapterHomeNotification.ViewHolder>{

    private static final String TAG = "HomeNotificationRecyclerViewAdapter";

    private ArrayList<String> mImageNames;
    private ArrayList<String> mImages;
    private Context mContext;


    public RecyclerViewAdapterHomeNotification(HomeActivity homeActivity , ArrayList<String> mNames , ArrayList<String> mImageUrls) {

        this.mContext=homeActivity;
        this.mImageNames = mNames;
        this.mImages = mImageUrls;

    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent , int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_listitem_notification, parent, false);
        ViewHolder holder = new ViewHolder (view);
        return holder;
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapterHomeNotification.ViewHolder holder , final int position) {

        Log.d(TAG, "onBindViewHolder: called.");

        Glide.with(mContext)
                .asBitmap()
                .load(mImages.get(position))
                .into(holder.imgProfile);

        holder.textImageProfile.setText(mImageNames.get(position));

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("onClick: clicked on: " + mImageNames.get(position) , TAG);

                Toast.makeText(mContext, mImageNames.get(position), Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(mContext, NotificationDetiailActivity.class);
                intent.putExtra("image_url", mImages.get(position));
                intent.putExtra("image_name", mImageNames.get(position));
                mContext.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return mImageNames.size();
    }

    public class ViewHolder  extends RecyclerView.ViewHolder {

        CircleImageView imgProfile;
        TextView textImageProfile;
        RelativeLayout parentLayout;

        public ViewHolder(View itemView) {
            super (itemView);
            imgProfile = itemView.findViewById(R.id.image);
            textImageProfile = itemView.findViewById(R.id.image_name);
            parentLayout = itemView.findViewById(R.id.parent_layout);
        }
    }

}












