package com.bts.app.mdsd.traveltrail.Notification;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bts.app.mdsd.traveltrail.BottomNavigationViewHelper;
import com.bts.app.mdsd.traveltrail.HomeActivity;
import com.bts.app.mdsd.traveltrail.Lastmint.LastActivityOfferMain;
import com.bts.app.mdsd.traveltrail.Profile.ProfileActivity;
import com.bts.app.mdsd.traveltrail.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class NotificationDetiailActivity extends AppCompatActivity {

    private static final String TAG = "GalleryActivity";
    private TextView mTextMessage;
    private Intent i;
    Button button;



    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    mTextMessage.setText(R.string.title_home);
                    i = new Intent (NotificationDetiailActivity.this , HomeActivity.class);
                    startActivity(i);
                    finish();
                    return true;

                case R.id.navigation_experience:
                    mTextMessage.setText(R.string.title_experience);
                    i = new Intent(NotificationDetiailActivity.this , ProfileActivity.class);
                    startActivity(i);
                    finish();
                    return true;

                case R.id.navigation_lastmint:
                    mTextMessage.setText(R.string.title_lastmintoffer);
                    i = new Intent(NotificationDetiailActivity.this , LastActivityOfferMain.class);
                    startActivity(i);
                    finish();
                    return true;

                case R.id.navigation_notifications:
                    mTextMessage.setText(R.string.title_notifications);
                    i = new Intent(NotificationDetiailActivity.this , NotificationActivity.class);
                    startActivity(i);
                    finish();

                    return true;

                case R.id.navigation_setting:
                    mTextMessage.setText(R.string.title_setting);
                    i = new Intent(NotificationDetiailActivity.this , HomeActivity.class);
                    startActivity(i);
                    finish();
                    return true;
            }
            return false;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_notification_detiail);
        Log.d(TAG, "onCreate: started.");

        mTextMessage = (TextView) findViewById(R.id.message);


        getIncomingIntent();

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        BottomNavigationViewHelper.disableShiftMode(navigation);
    }

    private void getIncomingIntent() {

        Log.d(TAG, "getIncomingIntent: checking for incoming intents.");

        if(getIntent().hasExtra("image_url") && getIntent().hasExtra("image_name")){
            Log.d(TAG, "getIncomingIntent: found intent extras.");

            String imageUrl = getIntent().getStringExtra("image_url");
            String imageName = getIntent().getStringExtra("image_name");

            setImage(imageUrl, imageName);
        }

    }

    private void setImage(String imageUrl , String imageName) {

        Log.d(TAG, "setImage: setting te image and name to widgets.");

        TextView name = findViewById(R.id.image_description);
        name.setText(imageName);

        ImageView image = findViewById(R.id.image);
        Glide.with(this)
                .asBitmap()
                .load(imageUrl)
                .into(image);
    }

    public static class RecyclerViewAdaptor extends RecyclerView.Adapter<RecyclerViewAdaptor.ViewHolder>{

        private static final String TAG = "Recycler View Adaptor";
        private ArrayList<String> mImageNames = new ArrayList<>();
        private ArrayList<String> mImages = new ArrayList<>();
        private Context mContext;

        public RecyclerViewAdaptor (Context context, ArrayList<String> imageNames, ArrayList<String> images ) {
            mImageNames = imageNames;
            mImages = images;
            mContext = context;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent , int viewType) {

            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_listitem_notification, parent, false);
            ViewHolder holder = new ViewHolder (view);
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder , final int position) {
            Log.d(TAG, "onBindViewHolder: called.");

            Glide.with(mContext)
                    .asBitmap()
                    .load(mImages.get(position))
                    .into(holder.imgProfile);

            holder.textImageProfile.setText(mImageNames.get(position));

            holder.parentLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d(TAG, "onClick: clicked on: " + mImageNames.get(position));

                    Toast.makeText(mContext, mImageNames.get(position), Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(mContext, NotificationDetiailActivity.class);
                    intent.putExtra("image_url", mImages.get(position));
                    intent.putExtra("image_name", mImageNames.get(position));
                    mContext.startActivity(intent);
                }
            });
        }


        @Override
        public int getItemCount() {
            return mImageNames.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            CircleImageView imgProfile;
            TextView textImageProfile;
            RelativeLayout parentLayout;

            public ViewHolder(View itemView) {
                super(itemView);
                imgProfile = itemView.findViewById(R.id.image);
                textImageProfile = itemView.findViewById(R.id.image_name);
                parentLayout = itemView.findViewById(R.id.parent_layout);

            }
        }

    }
}
