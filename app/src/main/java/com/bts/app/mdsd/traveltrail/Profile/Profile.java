package com.bts.app.mdsd.traveltrail.Profile;

public class Profile {

    private String name;
    private String type ;
    private String description ;
    private int thumbnail ;
    private int masterof;
    private int Avatar;


    public Profile(String name , String type , String description , int thumbnail , int masterof , int avatar) {
        this.name = name;
        this.type = type;
        this.description = description;
        this.thumbnail = thumbnail;
        this.masterof = masterof;
        this.Avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }


    public int getMasterof() {
        return masterof;
    }

    public void setMasterof(int masterof) {
        this.masterof = masterof;
    }

    public int getAvatar() {
        return Avatar;
    }

    public void setAvatar(int avatar) {
        Avatar = avatar;
    }
}
