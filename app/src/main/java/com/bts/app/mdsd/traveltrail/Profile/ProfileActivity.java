package com.bts.app.mdsd.traveltrail.Profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.bts.app.mdsd.traveltrail.BottomNavigationViewHelper;
import com.bts.app.mdsd.traveltrail.HomeActivity;
import com.bts.app.mdsd.traveltrail.Lastmint.LastActivityOfferMain;
import com.bts.app.mdsd.traveltrail.Notification.NotificationActivity;
import com.bts.app.mdsd.traveltrail.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

public class ProfileActivity extends AppCompatActivity {

    private TextView mTextMessage;
    private GoogleMap mMap;
    private Intent i;

    private static final String TAG = "ProfileActivity";

    //vars

    List<Profile> lstprofile ;

    private static final int NUM_COLUMNS = 2;
    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<String> mImageUrls = new ArrayList<>();

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            switch (item.getItemId()) {
                case R.id.navigation_home:
                    mTextMessage.setText(R.string.title_home);
                    i = new Intent(ProfileActivity.this, HomeActivity.class);
                    startActivity(i);
                    finish();
                    return true;

                case R.id.navigation_experience:
                    mTextMessage.setText(R.string.title_experience);
                    return true;

                case R.id.navigation_lastmint:
                    mTextMessage.setText(R.string.title_lastmintoffer);
                    i = new Intent(ProfileActivity.this, LastActivityOfferMain.class);
                    startActivity(i);
                    finish();
                    return true;

                case R.id.navigation_notifications:
                    mTextMessage.setText(R.string.title_notifications);
                    i = new Intent(ProfileActivity.this, NotificationActivity.class);
                    startActivity(i);
                    finish();
                    return true;

                case R.id.navigation_setting:
                    mTextMessage.setText(R.string.title_setting);
                    i = new Intent(ProfileActivity.this, ProfileActivity.class);
                    startActivity(i);
                    finish();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        getImages();

        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        BottomNavigationViewHelper.disableShiftMode(navigation);

    }

    private void getImages(){

        Log.d(TAG, "initImageBitmaps: preparing bitmaps.");

        lstprofile= new ArrayList<> ();
        lstprofile.add(new Profile ("John Rest", "Traveller", "Discovering my world", R.drawable.thevigitarian, R.drawable.ic_home_black_24dp , R.drawable.ic_home_black_24dp));
        lstprofile.add(new Profile ("Boban Parson", "Local", "Travelling for 1 year around the world,", R.drawable.thevigitarian, R.drawable.ic_home_black_24dp , R.drawable.ic_home_black_24dp));
        lstprofile.add(new Profile ("Mary Chilling", "Wonderlust", "Travelling for 1 year around the world,", R.drawable.thevigitarian, R.drawable.ic_home_black_24dp , R.drawable.ic_home_black_24dp));
        lstprofile.add(new Profile ("Jousha Ramada", "Traveler", "Travelling for 1 year around the world,", R.drawable.thevigitarian, R.drawable.ic_home_black_24dp , R.drawable.ic_home_black_24dp));
        lstprofile.add(new Profile ("Tommaso Crociera", "Traveler", "Travelling for 1 year around the world,", R.drawable.thevigitarian, R.drawable.ic_home_black_24dp , R.drawable.ic_home_black_24dp));
        lstprofile.add(new Profile ("Hilda Rames", "Traveller", "Travelling for 1 year around the world,", R.drawable.thevigitarian, R.drawable.ic_home_black_24dp , R.drawable.ic_home_black_24dp));
        lstprofile.add(new Profile ("Francesca Antani", "Local", "Travelling for 1 year around the world,", R.drawable.thevigitarian, R.drawable.ic_home_black_24dp , R.drawable.ic_home_black_24dp));
        lstprofile.add(new Profile ("Rwalda Bestinger", "Traveller", "Travelling for 1 year around the world,", R.drawable.thevigitarian, R.drawable.ic_home_black_24dp , R.drawable.ic_home_black_24dp));
        lstprofile.add(new Profile ("Camila Costa", "Wonderlust", "Travelling for 1 year around the world,", R.drawable.thevigitarian, R.drawable.ic_home_black_24dp , R.drawable.ic_home_black_24dp));
        lstprofile.add(new Profile ("Alejandro Bistrot", "Traveller", "Travelling for 1 year around the world,", R.drawable.thevigitarian, R.drawable.ic_home_black_24dp , R.drawable.ic_home_black_24dp));
        lstprofile.add(new Profile ("Sonia Kalamaka", "Wonderlust", "Travelling for 1 year around the world,", R.drawable.thevigitarian, R.drawable.ic_home_black_24dp , R.drawable.ic_home_black_24dp));
        lstprofile.add(new Profile ("Randy Marlon", "Traveller", "Travelling for 1 year around the world,", R.drawable.thevigitarian, R.drawable.ic_home_black_24dp , R.drawable.ic_home_black_24dp));
        lstprofile.add(new Profile ("Muccio Resto", "Local", "Travelling for 1 year around the world,", R.drawable.thevigitarian, R.drawable.ic_home_black_24dp , R.drawable.ic_home_black_24dp));
        lstprofile.add(new Profile ("Zeyad Ars", "Traveller", "Travelling for 1 year around the world,", R.drawable.thevigitarian, R.drawable.ic_home_black_24dp , R.drawable.ic_home_black_24dp));
        lstprofile.add(new Profile ("Luciana Scappella", "Traveller", "Travelling for 1 year around the world,", R.drawable.thevigitarian, R.drawable.ic_home_black_24dp , R.drawable.ic_home_black_24dp));
        lstprofile.add(new Profile ("Morgano Uomo Libero", "Local", "Travelling for 1 year around the world,", R.drawable.thevigitarian, R.drawable.ic_home_black_24dp , R.drawable.ic_home_black_24dp));
        lstprofile.add(new Profile ("Boris Resink", "Traveller", "Travelling for 1 year around the world,", R.drawable.thevigitarian, R.drawable.ic_home_black_24dp , R.drawable.ic_home_black_24dp));
        lstprofile.add(new Profile ("Admyly Adnan", "Wonderlust", "Travelling for 1 year around the world,", R.drawable.thevigitarian, R.drawable.ic_home_black_24dp , R.drawable.ic_home_black_24dp));
        lstprofile.add(new Profile ("Relegada23", "Traveller", "Travelling for 1 year around the world,", R.drawable.thevigitarian, R.drawable.ic_home_black_24dp , R.drawable.ic_home_black_24dp));
        lstprofile.add(new Profile ("ReggaeVibe93", "Traveller", "Travelling for 1 year around the world,", R.drawable.thevigitarian, R.drawable.ic_home_black_24dp , R.drawable.ic_home_black_24dp));
        lstprofile.add(new Profile ("ProudQuinoa", "Local", "Travelling for 1 year around the world,", R.drawable.thevigitarian, R.drawable.ic_home_black_24dp , R.drawable.ic_home_black_24dp));
        lstprofile.add(new Profile ("FullTimeTraveler", "Traveller", "Travelling for 1 year around the world,", R.drawable.thevigitarian, R.drawable.ic_home_black_24dp , R.drawable.ic_home_black_24dp));
        lstprofile.add(new Profile ("ComeFosseAntani12", "Traveller", "Travelling for 1 year around the world,", R.drawable.thevigitarian, R.drawable.ic_home_black_24dp , R.drawable.ic_home_black_24dp));
        lstprofile.add(new Profile ("Shamalaya", "Local", "Travelling for 1 year around the world,", R.drawable.thevigitarian, R.drawable.ic_home_black_24dp , R.drawable.ic_home_black_24dp));
        lstprofile.add(new Profile ("Rimastino12", "Traveller", "Travelling for 1 year around the world,", R.drawable.thevigitarian, R.drawable.ic_home_black_24dp , R.drawable.ic_home_black_24dp));



        RecyclerView myrv = (RecyclerView) findViewById(R.id.recyle_profile_activity);
        RecyclerViewProfileAdapter myAdapter = new RecyclerViewProfileAdapter (this,lstprofile);
        myrv.setLayoutManager(new GridLayoutManager(this,2));
        myrv.setAdapter(myAdapter);

    }

    private void initRecyclerView(){

        Log.d(TAG, "initRecyclerView: initializing staggered recyclerview.");

        RecyclerView recyclerView = findViewById(R.id.recyle_profile_activity);
        RecyclerViewProfileAdapter staggeredRecyclerViewAdapter =
                new RecyclerViewProfileAdapter (this,lstprofile);
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(NUM_COLUMNS, LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        recyclerView.setAdapter(staggeredRecyclerViewAdapter);

    }


}
