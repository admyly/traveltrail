package com.bts.app.mdsd.traveltrail.Profile;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bts.app.mdsd.traveltrail.R;

public class UserDetailProfile extends AppCompatActivity {

    private TextView txtUserName;
    private TextView txtUserType;
    private TextView txtUserTag;
    private TextView txtUserDescrption;
    private ImageView imgProfile;
    private ImageView imgTag;
    private ImageView imgAvatar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_user_detail_profile);


        txtUserName = (TextView) findViewById(R.id.txtProfileName);
        txtUserType = (TextView) findViewById(R.id.textUserType);
        txtUserTag = (TextView) findViewById(R.id.textUserTag);
        txtUserDescrption = (TextView) findViewById(R.id.textIntro);
        imgProfile = (ImageView) findViewById(R.id.imageprofile);
        imgTag = (ImageView) findViewById(R.id.imageUserTag);
        imgAvatar = (ImageView) findViewById(R.id.imageAvatar);

        // Recieve data

        Intent intent = getIntent();

        String name = intent.getExtras().getString("Name");
        String type = intent.getExtras().getString("type");

        String Description = intent.getExtras().getString("Description");

        int thumbnail = intent.getExtras().getInt("thumbnail") ;
        int masterof = intent.getExtras().getInt("masterof") ;
        int Avatar = intent.getExtras().getInt("Avatar") ;

        // Setting values

        txtUserName.setText(name);
        txtUserType.setText(type);
        txtUserDescrption.setText(Description);
        imgProfile.setImageResource (thumbnail);
        imgTag.setImageResource (masterof);
        imgAvatar.setImageResource (Avatar);

    }
}
